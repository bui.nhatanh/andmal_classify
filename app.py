import os
from os.path import join, dirname

from flask import Flask, flash, request, redirect, render_template
from flask.json import jsonify
from werkzeug.utils import secure_filename
from flask import send_from_directory

from model import transform

UPLOAD_FOLDER = join(dirname(__file__), "tmp", "upload")
ALLOWED_EXTENSIONS = "apk"

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/web/detector', methods=['GET', 'POST'])
def web_demo():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            save_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(save_path)
            print(save_path)
            output = transform(save_path)
            return render_template("VirusTotal_web.html", test=output)
    return render_template("index.html")


@app.route('/app/detector', methods=['GET', 'POST'])
def app_demo():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            save_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(save_path)
            print(save_path)
            output = transform(save_path)
            return render_template("app_virus_detector.html", test=output)
    return render_template("index.html")


@app.route('/api', methods=['GET', 'POST'])
def api():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            save_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(save_path)
            output = transform(save_path)
            return jsonify(output)
    return render_template("index.html")


@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename)


if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")
