import os
from os.path import join

from sklearn import metrics
from sklearn.metrics import classification_report

from model import model_predict


analyze = []
true_label = []
pred_label = []
cwd = os.getcwd()
path = cwd.replace("/egs/resnet", "/dataset/test")
targets_name = os.listdir(path)
for target in targets_name:
    target_true = []
    target_pred = []
    files = [join(path, target, i) for i in os.listdir(join(path, target))]
    for file in files:
        label = model_predict(file)
        true_label.append(target)
        pred_label.append(label)
    acc = metrics.accuracy_score(y_true=true_label, y_pred=pred_label)
    print("{} acc: ".format(target), acc)
acc = metrics.accuracy_score(y_true=true_label, y_pred=pred_label)
f1_score = metrics.f1_score(y_true=true_label, y_pred=pred_label, average="weighted")
print("Total Accuracy: ", acc)
print("Total F1: ", f1_score)
print(classification_report(true_label, pred_label))
