import json
from os.path import join, dirname

from keras.preprocessing.image import img_to_array
from keras.models import load_model
import numpy as np
import cv2

from utils.helpers import save_image, scanner, file_size


model_name = join(dirname(__file__), "model.bin")
model = load_model(model_name)


def model_predict(filename):
    label_id = join(dirname(__file__), "label_id.json")
    label_id = json.loads(open(label_id).read())
    image = cv2.imread(filename, 0)
    # image = cv2.resize(image, (32, 32))
    image = image.astype("float") / 255.0
    image = img_to_array(image)
    image = np.expand_dims(image, axis=0)
    predicted = model.predict(image)[0]
    y_pred = predicted.argmax(axis=0)
    malware_name = [i["name"] for i in label_id if y_pred == i["id"]][0]
    return malware_name


def malicious_detector(file_name):
    label_transfer = {
        "Airpush": "Android.Adware.Airpush",
        "Benign": "Android.Clean",
        "Dowgin": "Android.Adware.Dowgin",
        "FakeInst": "Android.Trojan-SMS.Fakeinst",
        "Fusob": "Android.Trojan-Ransom.Fusob ",
        "Kuguo": "Android.Adware.Kuguo",
        "Mecor": "Android.Trojan-Spy.Mecor",
        "Youmi": "Android.Adware.Youmi",
    }
    file = save_image(file_name)
    if file is not None:
        malware_name = model_predict(file)
        detect = True if malware_name != "Clean" else False
        malware_name = label_transfer.get(malware_name)
        return {
            "My Model": {"detected": detect,
                         "version": "vggnet",
                         "result": malware_name,
                         "update": "20190403"}
        }
    else:
        return {
            "My Model": {"detected": None,
                         "version": "vggnet",
                         "result": None,
                         "update": "20190403"}
        }


def transform(file_name):
    data = {}
    engines_center = ["McAfee", "Bkav", "CMC", "Baidu", "Kaspersky",
                      "Avast", "Panda", "Microsoft", "BitDefender"]
    engines_scan = scanner(filename=file_name)
    if engines_scan is not None and "scans" in list(engines_scan):
        engines = [{k: engines_scan["scans"].get(k)} for k in engines_center if
                   k in engines_scan["scans"]]
        cnn_predict = malicious_detector(file_name)
        engines.append(cnn_predict)

        data["name"] = file_name.split("/")[-1]
        data["file_size"] = file_size(file_name)
        data["scans"] = dict((key, d[key]) for d in engines for key in d)
        data["sha256"] = engines_scan["sha256"]
        data["time"] = engines_scan["scan_date"]
        data['total'] = len(data["scans"])
        data["positives"] = len([i for i in data["scans"].items() if i[1]["detected"] is True])
        return data


malicious_detector("/home/anhbn/lab/andmal_classify/tmp/upload/carefully_delete.apk")
