import time

from model import model_predict

label_transfer = {
        "Airpush": "Android.Adware.Airpush",
        "Benign": "Android.Clean",
        "Dowgin": "Android.Adware.Dowgin",
        "FakeInst": "Android.Trojan-SMS.Fakeinst",
        "Fusob": "Android.Trojan-Ransom.Fusob ",
        "Kuguo": "Android.Adware.Kuguo",
        "Mecor": "Android.Trojan-Spy.Mecor",
        "Youmi": "Android.Adware.Youmi",
    }

tic = time.time()
file = "/home/anhbn/lab/andmal_classify/dataset/test/Airpush/0a0de8870c3338dbd0392a2a9649491d.png"
estimated = model_predict(file)
# label = estimated["My Model"]["result"]
label = label_transfer.get(estimated)

print("Label:", label)
print("Time: ", time.time() - tic)
