
import json
from os.path import join, dirname

from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import Adam
from sklearn.model_selection import train_test_split
from keras.preprocessing.image import img_to_array
from keras.utils import to_categorical, plot_model
from imutils import paths
import numpy as np
import random
import cv2
import os
import matplotlib.pyplot as plt

from lenet import LeNet

EPOCHS = 20
INIT_LR = 1e-3
BS = 64

print("Loading images...")
data = []
labels = []
dir_labels = ()
num_class = 0
cwd = os.getcwd()
dataset = cwd.replace("/egs/lenet-5", "/dataset/images/train")
# print(dataset)
# exit()
save_model = join(dirname(__file__), "model", "model.bin")

print("[INFO] Finding Labels...")
for file in os.listdir(dataset):
    temp_tuple = (file, 'null')
    dir_labels = dir_labels + temp_tuple
    dir_labels = dir_labels[:-1]
    num_class = num_class + 1

image_paths = list(paths.list_images(dataset))
random.seed(42)
random.shuffle(image_paths)

for image_path in image_paths:
    image = cv2.imread(image_path, 0)
    # image = cv2.resize(image, (32, 32))
    image = img_to_array(image)
    data.append(image)

    label = image_path.split(os.path.sep)[-2]
    for i in range(num_class):
        if label == dir_labels[i]:
            name = image_path.split("/")[-2]
            label = {"id": i, "name": name}
    labels.append(label)
label_id = list({v['id']: v for v in labels}.values())
with open("model/label_id.json", "w") as f:
    json.dump(label_id, f)
data = np.array(data, dtype="float") / 255.0
labels = np.array([i["id"] for i in labels])
(x_train, x_test, y_train, y_test) = train_test_split(data, labels,
                                                      test_size=0.2,
                                                      random_state=42,
                                                      shuffle=True)

y_train = to_categorical(y_train, num_classes=num_class)
y_test = to_categorical(y_test, num_classes=num_class)

aug = ImageDataGenerator(rotation_range=30, width_shift_range=0.1,
                         height_shift_range=0.1, shear_range=0.2, zoom_range=0.2,
                         horizontal_flip=True, fill_mode="nearest")

num_class = len(list(set(labels)))
print("Compiling model...")
model = LeNet.build(width=59, height=59, depth=1, classes=num_class)
opt = Adam(lr=INIT_LR, decay=INIT_LR / EPOCHS)
model.compile(loss="categorical_crossentropy", optimizer=opt,
              metrics=["accuracy"])

print("Training network...")
H = model.fit_generator(aug.flow(x_train, y_train, batch_size=BS),
                        validation_data=(x_test, y_test), steps_per_epoch=len(x_train) // BS,
                        epochs=EPOCHS, verbose=1, shuffle=True)

print("Serializing network...")
model.save(save_model)
plot_model(model, to_file='analyze/model_plot.png', show_shapes=True, show_layer_names=True)

plt.style.use("ggplot")
plt.figure()

plt.title("Training Accuracy")
plt.plot(np.arange(0, EPOCHS), H.history["acc"], label="train_acc", marker='o')
plt.plot(np.arange(0, EPOCHS), H.history["val_acc"], label="val_acc", marker='^')
plt.xlabel("Epoch #")
plt.ylabel("Accuracy")
plt.legend(loc="lower left")
plt.savefig("analyze/train_accuracy.png")

plt.style.use("ggplot")
plt.figure()

plt.title("Training Loss")
plt.plot(np.arange(0, EPOCHS), H.history["loss"], label="train_loss", marker='o')
plt.plot(np.arange(0, EPOCHS), H.history["val_loss"], label="val_loss", marker='^')
plt.xlabel("Epoch #")
plt.ylabel("Loss")
plt.legend(loc="lower left")
plt.savefig("analyze/train_loss.png")
