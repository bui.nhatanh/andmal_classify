Fusob acc:  0.99609375
Youmi acc:  0.7388781431334622
Mecor acc:  0.8467650397275823
Benign acc:  0.8431048720066061
Airpush acc:  0.8515196811160937
Dowgin acc:  0.8517138599105812
Kuguo acc:  0.8334473324213406
FakeInst acc:  0.8550163739208098
Total Accuracy:  0.8550163739208098
Total F1:  0.8537952132463124
              precision    recall  f1-score   support

     Airpush       0.90      0.86      0.88       796
      Benign       0.83      0.83      0.83       330
      Dowgin       0.77      0.85      0.81       677
    FakeInst       0.98      1.00      0.99       435
       Fusob       0.99      1.00      0.99       256
       Kuguo       0.52      0.63      0.57       240
       Mecor       0.99      1.00      1.00       364
       Youmi       0.81      0.49      0.61       261

   micro avg       0.86      0.86      0.86      3359
   macro avg       0.85      0.83      0.84      3359
weighted avg       0.86      0.86      0.85      3359
