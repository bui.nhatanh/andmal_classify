Fusob acc:  0.99609375
Youmi acc:  0.4932301740812379
Mecor acc:  0.70261066969353
Benign acc:  0.5111478117258464
Airpush acc:  0.7030393622321873
Dowgin acc:  0.5257078986587184
Kuguo acc:  0.48255813953488375
FakeInst acc:  0.5495683239059244
Total Accuracy:  0.5495683239059244
Total F1:  0.43411097367914286
              precision    recall  f1-score   support

     Airpush       0.34      0.99      0.51       796
      Benign       0.00      0.00      0.00       330
      Dowgin       0.00      0.00      0.00       677
    FakeInst       0.98      1.00      0.99       435
       Fusob       1.00      1.00      1.00       256
       Kuguo       0.00      0.00      0.00       240
       Mecor       1.00      1.00      1.00       364
       Youmi       0.00      0.00      0.00       261

   micro avg       0.55      0.55      0.55      3359
   macro avg       0.42      0.50      0.44      3359
weighted avg       0.39      0.55      0.43      3359
