from keras.layers import BatchNormalization, Dropout
from keras.models import Sequential
from keras.layers.convolutional import Conv2D, ZeroPadding2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Activation
from keras.layers.core import Flatten
from keras.layers.core import Dense
from keras.regularizers import l2


class AlexNet:
    @staticmethod
    def build(height, width, depth, num_classes):
        l2_reg = 0

        model = Sequential()
        input_shape = (height, width, depth)

        # 1 CPR
        model.add(Conv2D(96, (11, 11), input_shape=input_shape, padding='same', kernel_regularizer=l2(l2_reg)))
        model.add(BatchNormalization())
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        # 2 CPR

        model.add(Conv2D(256, (5, 5), padding='same'))
        model.add(BatchNormalization())
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        # 3 CPR
        model.add(ZeroPadding2D((1, 1)))
        model.add(Conv2D(512, (3, 3), padding='same'))
        model.add(BatchNormalization())
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        # 4 CPR
        model.add(ZeroPadding2D((1, 1)))
        model.add(Conv2D(1024, (3, 3), padding='same'))
        model.add(BatchNormalization())
        model.add(Activation('relu'))

        # 5 CPR
        model.add(ZeroPadding2D((1, 1)))
        model.add(Conv2D(1024, (3, 3), padding='same'))
        model.add(BatchNormalization())
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Flatten())

        # 1 FC

        model.add(Dense(3072))
        model.add(BatchNormalization())
        model.add(Activation('relu'))
        model.add(Dropout(0.5))

        # 2 FC

        model.add(Dense(4096))
        model.add(BatchNormalization())
        model.add(Activation('relu'))
        model.add(Dropout(0.5))

        # 3 FC

        model.add(Dense(num_classes, activation='softmax'))
        model.add(BatchNormalization())
        model.add(Activation('softmax'))

        return model
