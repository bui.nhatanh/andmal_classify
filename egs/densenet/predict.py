import argparse
import time

from model import malicious_detector

label_transfer = {
        "Airpush": "Android.Adware.Airpush",
        "Benign": "Android.Clean",
        "Dowgin": "Android.Adware.Dowgin",
        "FakeInst": "Android.Trojan-SMS.Fakeinst",
        "Fusob": "Android.Trojan-Ransom.Fusob ",
        "Kuguo": "Android.Adware.Kuguo",
        "Mecor": "Android.Trojan-Spy.Mecor",
        "Youmi": "Android.Adware.Youmi",
    }

tic = time.time()
file = "/home/anhbn/lab/andmal_classify/tmp/upload/0ad1f93619e9908cdcaefc8b2ddcfc1e.apk"
estimated = malicious_detector(file)
label = estimated["My Model"]["result"]
label = [key for key, value in label_transfer.items() if value == label][0]

print("Label:", label)
print("Time: ", time.time() - tic)