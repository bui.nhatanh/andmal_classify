import os
from os.path import join, dirname

from matplotlib import cm
import numpy as np
import matplotlib.image as mplimg
import requests
from androguard.core.bytecodes import apk, dvm
from androguard.session import Session

feature_name = join(dirname(__file__), "features.txt")
permissions = open(feature_name).read().split("\n")


def get_permission(filename):
    try:
        a = apk.APK(filename)
        permission = a.get_permissions()
        return permission
    except Exception:
        print("File error: ", filename)


def get_dex_feature(filename):
    a = apk.APK(filename)
    d = dvm.DalvikVMFormat(a.get_dex())
    z = d.get_strings()
    return z


def get_intent(dex):
    intents = []
    for i in range(len(dex)):
        if dex[i].startswith('android.intent.action'):
            intents.append(dex[i])
    return intents


def get_commands(dex):
    commands = []
    suspicious_cmds = ["su", "mount", "reboot", "mkdir", "chmod",
                       "sh", "killall", "reboot", "ln", "ps"]
    for i in range(len(dex)):
        for j in range(len(suspicious_cmds)):
            if suspicious_cmds[j] == dex[i]:
                commands.append(suspicious_cmds[j])
    return commands


def get_api_calls(dex):
    apis = []
    suspicious_apis = ["openFileOutput", "sendTextMessage", "getPackageManager",
                       "getDeviceId", "Runtime.exec", "Cipher.getInstance"]
    for i in range(len(dex)):
        for j in range(len(suspicious_apis)):
            if suspicious_apis[j] == dex[i]:
                apis.append(suspicious_apis[j])
    return apis


def scanner(filename):
    url = 'https://www.virustotal.com/vtapi/v2/file/report'
    apikey = "289b461d856beaf696d607a22794de4ec306db257bfccbbc7329baad97eaf5f0"
    try:
        s = Session()
        digest = s.add(filename)
        params = {'apikey': apikey, 'resource': digest}
        response = requests.get(url, params=params)
        output = response.json()
        return output
    except Exception as e:
        return None


def convert_bytes(num):
    """
    this function will convert bytes to MB.... GB... etc
    """
    for x in ['bytes', 'KB', 'MB', 'GB', 'TB']:
        if num < 1024.0:
            return "%3.1f %s" % (num, x)
        num /= 1024.0


def file_size(filename):
    """
    this function will return the file size
    """
    if os.path.isfile(filename):
        file_info = os.stat(filename)
        return convert_bytes(file_info.st_size)


def save_image(file_name):
    vector = convert_package(file_name)
    if vector is not None:
        image_file = file_name.replace("upload", "image").replace("apk", "png")
        mplimg.imsave(image_file, np.uint8(vector), cmap=cm.gray)
        return image_file


def convert_package(file_name):
    try:
        feature = get_permission(file_name)
        row = {}
        for permission in permissions:
            if permission in feature:
                row[permission] = 1
            else:
                row[permission] = 0
        data = np.array([value for key, value in row.items()])[:3481]
        vector = np.reshape(data, (59, 59))
        return vector
    except Exception as e:
        pass
