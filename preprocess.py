import glob
import os
from os.path import join, dirname

import numpy as np
import matplotlib.image as mplimg
from matplotlib import cm

from utils.helpers import convert_package


def convert_to_dataset(files, name):
    for file in files:
        vector = convert_package(file)
        if vector is not None:
            family = file.split("/")[-2]
            directory = join(dirname(__file__), "image_dataset", name, family)
            if not os.path.exists(directory):
                os.mkdir(directory)
            file_name = file.split("/")[-1].replace(".apk", ".png")
            image_file = join(directory, file_name)
            mplimg.imsave(image_file, np.uint8(vector), cmap=cm.gray)


amd_path = join(dirname(__file__), "android_malware_dataset")
folders = [i for i in os.listdir(amd_path) if i != '.gitignore']
for folder in folders:
    packages = glob.glob("{}/*.apk".format(join(amd_path, folder)))
    train_size = int(len(packages) * 0.8)
    train_files = packages[:train_size]
    test_files = packages[train_size:]
    convert_to_dataset(train_files, name="train")
    convert_to_dataset(test_files, name="test")
print("Finish")
